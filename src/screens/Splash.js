import React, {useEffect} from 'react';
import auth from '@react-native-firebase/auth';
import PropTypes from 'prop-types';
import {
  Dimensions,
  StyleSheet,
  Animated,
  View,
  Image,
  StatusBar,
  Easing
} from 'react-native';

const { height, width } = Dimensions.get('window');

const white = 'black';

const styles = StyleSheet.create({
  brand: {
    height: height,
    width: width,
    flex:1,
    resizeMode: "cover"
  },
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    backgroundColor: white,
  },
  splash: {
    alignItems: 'center',
    backgroundColor: white,
    height: 0,
    justifyContent: 'center',
    overflow: 'hidden',
    width: 0
  }
});

const Splash = (props) => {
  props = {
    delay: 600,
    onFinished: () => null,
    ...props
  };

  const splash = new Animated.Value(0);

  const splashStyle = {
    width: splash.interpolate({
      inputRange: [0, 1],
      outputRange: [0, width]
    }),
    height: splash.interpolate({
      inputRange: [0, 1],
      outputRange: [0, height]
    }),
    borderRadius: splash.interpolate({
      inputRange: [0, 1],
      outputRange: [height /2, 0]
    })
  }

  const animate = (value) => {
    return new Promise(resolve => {
      Animated.timing(
        splash,
        {
          useNativeDriver: false,
          toValue: value,
          duration: 600,
          easing: Easing.out(Easing.quad)
        }
      ).start(resolve);
    })
  }

  const onAuthStateChanged = (user) => {
    if(user == null) {
      auth().signInAnonymously();
    }

    const { delay, onFinished } = props;
    setTimeout(onFinished, delay)
  }

  useEffect(() => {
    animate(1);

    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return () => subscriber ;
    
  }, []);

  return (
      <>
          <StatusBar hidden />
          <View style={styles.container}>
              <Animated.View style={[styles.splash, splashStyle]}>
              <Image
                  source={require('../assets/splash.png')}
                  style={styles.brand}
              />
              </Animated.View>
          </View>
      </>
  );
}

Splash.propTypes = {
  delay: PropTypes.number,
  onFinished: PropTypes.func
};

export default Splash;