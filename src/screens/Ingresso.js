import React from 'react';
import {
  StyleSheet,
  ImageBackground,
  Dimensions,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native';
import QRCode from 'react-native-qrcode-svg';
import { useSelector } from 'react-redux';
import { getUser } from '../features/user/userSlice';

import { Block, Text, Button as GaButton, theme } from 'galio-framework';

import { Button } from '../components';
import { images, apptheme } from '../Constants';

const { width, height } = Dimensions.get('screen');

const DismissKeyboard = ({ children }) => (
  <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>{children}</TouchableWithoutFeedback>
);

const Ingresso = () => {
  const user = useSelector(getUser);

  return (
    <DismissKeyboard>
      <Block flex middle>
        <ImageBackground
          source={images.IngressoBackground}
          style={styles.imageBackgroundContainer}
          imageStyle={styles.imageBackground}
        >
          <Block flex middle>
            <Block style={styles.registerContainer}>
              <Block flex space="evenly">
                <Block flex={0.6} middle style={styles.socialConnect}>
                  <Block flex={1} middle>
                    <Text
                      style={{
                        fontFamily: 'montserrat-regular',
                        textAlign: 'center'
                      }}
                      color="#333"
                      size={24}
                    >
                      Check-in
                    </Text>
                  </Block>

                  <Block center flex={1} space="between">
                    <Block>
                      <QRCode
                        value={user.uid}
                      />
                    </Block>
                  </Block>

                </Block>
                
                <Block flex={0.4} middle space="between">
                  <Block center flex={0.7}>
                    <Block flex space="between">
                      
                      <Block center>
                        <Button onPress={()=>alert("Ainda não implementado")} color="primary" round style={styles.createButton}>
                          <Text
                            style={{ fontFamily: 'montserrat-bold' }}
                            size={14}
                            color={apptheme.COLORS.WHITE}
                          >
                            Registrar
                          </Text>
                        </Button>
                      </Block>
                    </Block>
                  </Block>
                  <Block flex={0.5} row middle space="between" style={{ marginBottom: 18 }}>
                    <GaButton
                      round
                      onlyIcon
                      shadowless
                      icon="facebook"
                      iconFamily="Font-Awesome"
                      iconColor={theme.COLORS.WHITE}
                      iconSize={theme.SIZES.BASE * 1.625}
                      color={apptheme.COLORS.FACEBOOK}
                      style={[styles.social, styles.shadow]}
                      onPress={()=>alert("Ainda não implementado")}
                    />

                    <GaButton
                      round
                      onlyIcon
                      shadowless
                      icon="google"
                      iconFamily="Font-Awesome"
                      iconColor={theme.COLORS.WHITE}
                      iconSize={theme.SIZES.BASE * 1.625}
                      color={apptheme.COLORS.GOOGLE}
                      style={[styles.social, styles.shadow]}
                      onPress={()=>alert("Ainda não implementado")}
                    />
                    
                    <GaButton
                      round
                      onlyIcon
                      shadowless
                      icon="twitter"
                      iconFamily="Font-Awesome"
                      iconColor={theme.COLORS.WHITE}
                      iconSize={theme.SIZES.BASE * 1.625}
                      color={apptheme.COLORS.TWITTER}
                      style={[styles.social, styles.shadow]}
                      onPress={()=>alert("Ainda não implementado")}
                    />

                  </Block>
                </Block>
              </Block>
            </Block>
          </Block>
        </ImageBackground>
      </Block>
    </DismissKeyboard>
  );
}

const styles = StyleSheet.create({
  imageBackgroundContainer: {
    width: width,
    height: height,
    padding: 0,
    zIndex: 1,
    backgroundColor: apptheme.COLORS.BLACK
  },
  imageBackground: {
    width: width,
    height: height,
    opacity: 0.5
  },
  registerContainer: {
    marginTop: 90,
    width: width * 0.9,
    height: (height < 812 ? height * 0.8 : height * 0.8)-90,
    backgroundColor: apptheme.COLORS.WHITE,
    borderRadius: 4,
    shadowColor: apptheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1,
    overflow: 'hidden'
  },
  socialConnect: {
    backgroundColor: apptheme.COLORS.WHITE
    // borderBottomWidth: StyleSheet.hairlineWidth,
    // borderColor: "rgba(136, 152, 170, 0.3)"
  },
  socialButtons: {
    width: 120,
    height: 40,
    backgroundColor: '#fff',
    shadowColor: apptheme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4
    },
    shadowRadius: 8,
    shadowOpacity: 0.1,
    elevation: 1
  },
  socialTextButtons: {
    color: apptheme.COLORS.PRIMARY,
    fontWeight: '800',
    fontSize: 14
  },
  inputIcons: {
    marginRight: 12,
    color: apptheme.COLORS.ICON_INPUT
  },
  inputs: {
    borderWidth: 1,
    borderColor: '#E3E3E3',
    borderRadius: 21.5
  },
  passwordCheck: {
    paddingLeft: 2,
    paddingTop: 6,
    paddingBottom: 15
  },
  createButton: {
    width: width * 0.5,
    marginTop: 25,
    marginBottom: 40
  },
  social: {
    width: theme.SIZES.BASE * 3.5,
    height: theme.SIZES.BASE * 3.5,
    borderRadius: theme.SIZES.BASE * 1.75,
    justifyContent: 'center',
    marginHorizontal: 10
  }
});

export default Ingresso;

