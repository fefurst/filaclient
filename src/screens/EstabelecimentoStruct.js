import React from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Dimensions, ScrollView, Image, ImageBackground, Alert } from 'react-native';
import { Block, Text, theme, Button as GaButton } from 'galio-framework';

import { Button } from '../components';
import { images, apptheme, utils } from '../Constants';


const { width, height } = Dimensions.get('screen');

const thumbMeasure = (width - 48 - 32) / 3;

var EstabelecimentoStruct = (props) => {

    const { width, height } = Dimensions.get('screen');

    return (
      <Block style={{
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }} >
        <Block flex={0.6} >
          <ImageBackground
            source={props.imagembg}
            style={styles.estabelecimentoContainer}
            imageStyle={styles.estabelecimentoBackground}
          >
            <Block flex style={styles.profileCard}>
              <Block style={{ position: 'absolute', width: width, zIndex: 5, paddingHorizontal: 20 }}>
                <Block middle style={{ top: height * 0.15 }}>
                  <Image source={props.imagempr} style={styles.avatar} />
                </Block>
                <Block style={{ top: height * 0.15 }}>
                  <Block middle >
                    <Text
                      style={{
                        fontFamily: 'montserrat-bold',
                        marginBottom: theme.SIZES.BASE / 2,
                        fontWeight: '900',
                        fontSize: 26
                      }}
                      color='#ffffff'
                      >
                      {props.nome}
                    </Text>

                    <Text
                      size={16}
                      color="white"
                      style={{
                        marginTop: 5,
                        fontFamily: 'montserrat-bold',
                        lineHeight: 20,
                        fontWeight: 'bold',
                        fontSize: 18,
                        opacity: .8
                      }}
                    >

                      {
                        props.posicaoNaFila != null
                        ?
                        `Você é o número #${props.posicaoNaFila} na fila`
                        :
                        (props.aberto?"Aberto":"Fechado")
                      }
                    </Text>
                  </Block>
                  <Block style={styles.info}>
                    <Block row space="around">

                      <Block middle>
                        <Text
                          size={18}
                          color="white"
                          style={{ marginBottom: 4, fontFamily: 'montserrat-bold' }}
                        >
                          {`${props.mesas - (props.mesa||[]).filter((m)=>m.idpessoa!=null).length} de ${props.mesas}`}
                        </Text>
                        <Text style={{ fontFamily: 'montserrat-regular' }} size={14} color="white">
                          Mesas disponíveis
                        </Text>
                      </Block>

                      <Block middle>
                        <Text
                          color="white"
                          size={18}
                          style={{ marginBottom: 4, fontFamily: 'montserrat-bold' }}
                        >
                          {(props.fila||[]).length}
                        </Text>
                        <Text style={{ fontFamily: 'montserrat-regular' }} size={14} color="white">
                          Pessoas na Fila
                          </Text>
                      </Block>

                    </Block>
                  </Block>
                </Block>

              </Block>
            </Block>
          </ImageBackground>
        </Block>
        <Block flex={0.4} style={{padding: theme.SIZES.BASE, marginTop: 80 }}>
          <Block
                  middle
                  row
                  style={{ position: 'absolute', height: 44, top: -22, width: width, zIndex: 4 }}
                >
                  {
                    (props.mostraBotao) ? (
                      (props.mostraSair)
                      ? 
                        (<Button onPress={()=>{props.funSairDaFila(props.idEmpresa, props.fila);}} style={{ backgroundColor: apptheme.COLORS.WARNING, width: 130, height: 44, marginHorizontal: 5, elevation: 2 }} textStyle={{ fontSize: 16 }} round>
                          Sair da fila
                        </Button>)
                      :
                      (<Button onPress={()=>{props.funEntrarNaFila(props.idEmpresa, props.fila);}} style={{ width: 170, height: 44, marginHorizontal: 5, elevation: 2 }} textStyle={{ fontSize: 16 }} round>
                        Entrar na Fila
                      </Button>)      
                    ) : <></>              
                  }             
          </Block>
          <ScrollView style={{borderRadius:5, backgroundColor:apptheme.COLORS.WHITE}} showsVerticalScrollIndicator={false}>
            <Block flex style={{ marginTop: 0 }}>
              <Block middle>
                <Text
                  style={{
                    color: '#2c2c2c',
                    fontWeight: 'bold',
                    fontSize: 19,
                    fontFamily: 'montserrat-bold',
                    marginTop: 15,
                    marginBottom: 30,
                    zIndex: 2
                  }}
                >
                  Sobre
                    </Text>
                <Text
                  size={16}
                  muted
                  style={{
                    textAlign: 'center',
                    fontFamily: 'montserrat-regular',
                    zIndex: 2,
                    lineHeight: 25,
                    color: '#9A9A9A',
                    paddingHorizontal: 15
                  }}
                >
                  {props.sobre}
                    </Text>
              </Block>           
            </Block>
          </ScrollView>
        </Block>
      </Block>
    )
}


const styles = StyleSheet.create({

  estabelecimentoContainer: {
    width,
    height,
    padding: 0,
    //zIndex: 1,
    overflow: "hidden",
    backgroundColor: "#000000"
  },
  estabelecimentoBackground: {
    width,
    height:  (height - utils.StatusHeight) * 0.7,
    opacity:0.5,
  },

  info: {
    marginTop: 10,
    paddingHorizontal: 10,
    height: height * 0.8
  },
  avatarContainer: {
    position: 'relative',
    marginTop: -80
  },
  avatar: {
    width: thumbMeasure,
    height: thumbMeasure,
    borderRadius: 50,
    borderWidth: 0
  },
  nameInfo: {
    marginTop: 35
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
  social: {
    width: apptheme.SIZES.BASE * 3,
    height: apptheme.SIZES.BASE * 3,
    borderRadius: apptheme.SIZES.BASE * 1.5,
    justifyContent: 'center',
    zIndex: 99,
    marginHorizontal: 5
  }
});

EstabelecimentoStruct.propTypes = {
  nome: PropTypes.string,
  imagembg: PropTypes.object,
  imagempr: PropTypes.object,
  user: PropTypes.object,
  posicaoNaFila: PropTypes.number,
  aberto: PropTypes.bool,
  mesa: PropTypes.array,
  mesas: PropTypes.number,
  fila: PropTypes.array,
  mostraBotao: PropTypes.bool,
  mostraSair: PropTypes.bool,
  funSairDaFila: PropTypes.func,
  funEntrarNaFila: PropTypes.func,
  sobre: PropTypes.string,
  idEmpresa: PropTypes.string
};

export default EstabelecimentoStruct;
