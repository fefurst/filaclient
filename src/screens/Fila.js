import React, { useState, useEffect } from 'react';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';

import { ScrollView, StyleSheet } from 'react-native';
import { Block, theme } from 'galio-framework';

import { apptheme } from '../Constants';
import { Card } from '../components/';

var renderCards = (empresas) => {
  return (
    <Block style={styles.container}>
      {
        Object.keys(empresas).map(
          (o, i) => {
          
            const refImagemPr = storage().ref(empresas[o].imagempr);

            return <Card key={i} item={
              {
                title: empresas[o].nome,
                image: refImagemPr.getDownloadURL(),
                subtitle: empresas[o].aberto?"Aberto":"Fechado",
                description: `${(empresas[o].fila||[]).length} pessoas na fila`,
                horizontal: true,
                data: o
              }
            } horizontal />
          }
        )
      }        
    </Block>
  );
};

function Fila() {  
  const [empresas, setEmpresas] = useState([]);

  useEffect(() => {
    const onValueChange = database()
      .ref(`/empresas`)
      .on('value', snapshot => {
          setEmpresas(snapshot.val());
      });

    return () => { 
      database()
        .ref(`/empresas`)
        .off('value', onValueChange);
    }
  }, []);

  return (
    <Block flex={1}>
      <ScrollView showsVerticalScrollIndicator={false}>{renderCards(empresas)}</ScrollView>
    </Block>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: theme.SIZES.BASE
  },
  title: {
    fontFamily: 'montserrat-bold',
    paddingBottom: theme.SIZES.BASE,
    marginTop: 44,
    color: apptheme.COLORS.HEADER
  }
});

export default Fila;
