import React, { useState, useEffect } from 'react';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import { useSelector } from 'react-redux';
import { getUser } from '../features/user/userSlice';
import { getUserEmpresa } from '../features/userEmpresa/userEmpresaSlice';
import EstabelecimentoStruct from './EstabelecimentoStruct';


var Estabelecimento = (props) => {
    const [data, setData] = useState({mesa:[], fila:[]});
    const [idEmpresa, setIdEmpresa] = useState((props.route.params||{data:null}).data);
    const [imagempr, setImagempr] = useState({uri:"about:blank"});
    const [imagembg, setImagembg] = useState({uri:"about:blank"}); 
    const user = useSelector(getUser);
    const userEmpresa = useSelector(getUserEmpresa);

    useEffect(() => {
      const onValueChange = database()
        .ref(`/empresas/${idEmpresa}`)
        .on('value', snapshot => {
            if(snapshot.val() == null) {
              setData({
                "aberto" : false,
                "id" : "0",
                "imagembg" : "https://i.vimeocdn.com/portrait/12134307_100x100",
                "imagempr" : "https://i.vimeocdn.com/portrait/12134307_100x100",
                "mesa" : [],
                "fila" : [],
                "mesas" : 0,
                "nome" : "Não está em fila alguma",
                "sobre" : "Entre em uma fila nos Estabelecimentos"
              });
              setImagempr(null);
              setImagembg(null);
            }
            else {
              setData(snapshot.val());
              storage().ref(snapshot.val().imagempr).getDownloadURL().then((o)=>{setImagempr({uri:o})});
              storage().ref(snapshot.val().imagembg).getDownloadURL().then((o)=>{setImagembg({uri:o})});
            }
        });     
      

      return () => { 
          database()
            .ref(`/empresas/${idEmpresa}`)
            .off('value', onValueChange);
      }


    }, []);

    const EntrarNaFila = (id, fila) => {
      database()
            .ref(`/empresas/${id}/fila/${(fila||[]).length}`)
            .set( {"idpessoa": user.uid} )
            .then(() => {
              database()
                .ref('/usuarios/'+user.uid)
                .set(id)
                .then(() => {});
            });
    };

    const SairDaFila = (id, fila) => {
      database()
        .ref(`/empresas/${id}/fila/${fila.map((d)=>d.idpessoa).indexOf(user.uid)}`)
        .set(null)
        .then(() => {
          database()
            .ref('/usuarios/'+user.uid)
            .set(null)
            .then(() => {})});
      
    };


    let dados, imgbg, imgpr, id;
    if(idEmpresa != null) {
      dados = data;
      imgbg = imagembg;
      imgpr = imagempr; 
      id = idEmpresa||(userEmpresa||{id:null}).id
    }
    else if(userEmpresa.id != null) {
      dados = userEmpresa.data;
      imgbg = userEmpresa.imagembg;
      imgpr = userEmpresa.imagempr; 
      id = userEmpresa.id
    }
    else {
      dados = {
        "aberto" : false,
        "id" : "0",
        "imagembg" : "https://i.vimeocdn.com/portrait/12134307_100x100",
        "imagempr" : "https://i.vimeocdn.com/portrait/12134307_100x100",
        "mesa" : [],
        "fila" : [],
        "mesas" : 0,
        "nome" : "Não está em fila alguma",
        "sobre" : "Entre em uma fila nos Estabelecimentos"
      };
      imgbg = null;
      imgpr = null;
      id = null;
    }

    return <EstabelecimentoStruct
        aberto={dados.aberto}
        fila={dados.fila}
        funEntrarNaFila={EntrarNaFila}
        funSairDaFila={SairDaFila}
        imagembg={imgbg}
        imagempr={imgpr}
        mesa={dados.mesa}
        mesas={dados.mesas}
        mostraBotao={dados.aberto && ((userEmpresa.id == idEmpresa || (userEmpresa.id != null && idEmpresa == null)) || userEmpresa.id == null)  }
        mostraSair={userEmpresa.id == idEmpresa || (userEmpresa.id != null && idEmpresa == null)  }
        nome={dados.nome}
        posicaoNaFila={((dados.fila||[]).map((d)=>d.idpessoa).indexOf(  (user||{uid:-1}).uid  ) > -1)?(dados.fila||[]).map((d)=>d.idpessoa).indexOf(  (user||{uid:-1}).uid  )+1:null}
        user={user}
        sobre={dados.sobre}
        idEmpresa={id}
    />

}


export default Estabelecimento;
