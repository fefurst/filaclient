import React from 'react';
import { Dimensions } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";

// telas
import Estabelecimento from '../screens/Estabelecimento';
import Fila from '../screens/Fila';
import Ingresso from '../screens/Ingresso';
// menu
import CustomDrawerContent from "./Menu";

import { Header } from '../components';
import { apptheme } from "../Constants";

const { width } = Dimensions.get("screen");

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function IngressoStack(props) {
  return (
    <Stack.Navigator initialRouteName="Check-in" mode="card" headerMode="screen">
      <Stack.Screen
          name="Check-in" 
          component={Ingresso} 
          options={{
            header:({ navigation, scene }) => (
              <Header 
                transparent 
                white 
                title="" 
                navigation={navigation} 
                scene={scene} 
              />
            ),
            cardStyle: { backgroundColor: "#FFFFFF" },
            headerTransparent: true
          }}/>
    </Stack.Navigator>
  );
}

function FilaStack(props) {

  return (
    <Stack.Navigator initialRouteName="Estabelecimentos" mode="card" headerMode="screen">
      <Stack.Screen 
          name="Estabelecimentos" 
          component={Fila} 
          options={{
            header: ({ navigation, scene }) => (
              <Header 
                bgColor={apptheme.COLORS.HEADER} 
                white 
                title="Estabelecimentos" 
                navigation={navigation} 
                scene={scene} 
              />
            ),
            backgroundColor: '#FFFFFF'
          }} />
      <Stack.Screen 
          name="Estabelecimento" 
          component={Estabelecimento} 
          options={{
            header: ({ navigation, scene }) => (
              <Header 
                transparent
                white 
                back
                title="" 
                navigation={navigation} 
                scene={scene} 
              />
            ),
            cardStyle: { backgroundColor: "#FFFFFF" },
            headerTransparent: true
          }} />
    </Stack.Navigator>
  );
}

function EstabelecimentoStack(props) {
  return (
    <Stack.Navigator initialRouteName="Fila" mode="card" headerMode="screen">
      <Stack.Screen
        name="Fila"
        component={Estabelecimento}
        props={props}
        
        options={{
          header: ({ navigation, scene }) => (
            <Header
              transparent
              white
              title=""
              navigation={navigation}
              scene={scene}
            />
          ),
          cardStyle: { backgroundColor: "#FFFFFF" },
          headerTransparent: true
        }}
      />
    </Stack.Navigator>
  );
}

const AppStack = (props) => {
  
  return (
    <Drawer.Navigator
      style={{ flex: 1 }}
      drawerContent={props => <CustomDrawerContent {...props} />}
      drawerStyle={{
        backgroundColor: apptheme.COLORS.PRIMARY,
        width: width * 0.8
      }}
      drawerContentOptions={{
        activeTintcolor: apptheme.COLORS.WHITE,
        inactiveTintColor: apptheme.COLORS.WHITE,
        activeBackgroundColor: "transparent",
        itemStyle: {
          width: width * 0.75,
          backgroundColor: "transparent",
          paddingVertical: 16,
          paddingHorizonal: 12,
          justifyContent: "center",
          alignContent: "center",
          alignItems: "center",
          overflow: "hidden"
        },
        labelStyle: {
          fontSize: 18,
          marginLeft: 12,
          fontWeight: "normal"
        }
      }}
      initialRouteName="Estabelecimentos"
    >
      <Drawer.Screen name="Fila" component={EstabelecimentoStack} />
      <Drawer.Screen name="Estabelecimentos" component={FilaStack} />
      <Drawer.Screen name="Check-in" component={IngressoStack} />
    </Drawer.Navigator>
  );
}

export default AppStack;
