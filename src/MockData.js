export const MockFilaEstabelecimento = {
    "id": "asd12001",
    "nome":"Boteco Silva",
    "imagembg":require('./assets/imgs/bgEstab.png'),
    "imagempr":require('./assets/imgs/comida2.jpg'),
    "aberto": true,
    "mesas":16,
    "mesasDisponiveis": 6,
    "sobre":"A renomada gastronomia da Família Silva é reconhecida mundialmente por seus quitutes de bar, com um tempero sem igual você poderá desfrutar de iguarias enquando bebe um gelado chope Astromélia Oak de fabricação própria.",
    "pessoasNaFila": 7,
    "posicaoNaFila": 5
};

export const MockEstabelecimentos = [
    {
        "id":"asd12001",
        "nome":"Boteco Silva",
        "imagembg":require('./assets/imgs/bgEstab.png'),
        "imagempr":require('./assets/imgs/comida2.jpg'),
        "aberto": false,
        "mesas":13,
        "mesasDisponiveis": 13,
        "sobre":"A renomada gastronomia da Família Silva é reconhecida mundialmente por seus quitutes de bar, com um tempero sem igual você poderá desfrutar de iguarias enquando bebe um gelado chope Astromélia Oak de fabricação própria.",
        "pessoasNaFila": 7,
        "posicaoNaFila": null
    },
    {
        "id":"asd12001",
        "nome":"Restaurante Tremelin",
        "imagembg":{uri:"https://static3.depositphotos.com/1009905/269/i/450/depositphotos_2690303-stock-photo-interior-of-the-restaurant.jpg"},
        "imagempr":{uri:"https://img.elo7.com.br/product/original/22565B3/adesivo-parede-prato-comida-frango-salada-restaurante-lindo-adesivo-parede.jpg"},
        "aberto": true,
        "mesas":16,
        "mesasDisponiveis": 16,
        "sobre":"Chegado num rango de primeira? Aqui no restarurante Tremelin prezamos pela experiência de sabor mais recompesadora para os nossos clientes. Com pratos variados e temperos especiais, proporcionamos o melhor da culinária da região.",
        "pessoasNaFila": 7,
        "posicaoNaFila": null
    },
    {
        "id":"asf12003",
        "nome":"Bar do Tião",
        "imagembg":{uri:"https://blog.saipos.com/wp-content/uploads/2018/11/Como-administrar-um-bar-e-restaurante-SAIPOS-Sistema-para-Restaurantes.jpg"},
        "imagempr":{uri:"https://img.freepik.com/fotos-gratis/bebida-com-alcool-saborosa-com-tequila-de-cocktail-com-lima-e-sal-no-fundo-escuro-vibrante-fechar-se-horizontal_1220-1523.jpg?size=626&ext=jpg"},
        "aberto": false,
        "mesas":5,
        "mesasDisponiveis": 5,
        "sobre":"Somos assassinos contratados para uma única missão, matar a sua sede. Nossas armas? Uma variedade regional de chope artesanal das melhores cervejarias. Prove também os licores e drinks especiais em nosso \"all inclusive happy hour\"!",
        "pessoasNaFila": 0,
        "posicaoNaFila": null
    },
    {
        "id":"awf12004",
        "nome":"Barbearia Macaquinho Sapeca",
        "imagembg":{uri:"https://diaonline.ig.com.br/wp-content/uploads/2018/10/11-barbearias-em-goiania-com-estilo-vintage-que-voce-precisa-conhecer.jpeg"},
        "imagempr":{uri:"https://i0.wp.com/blog.beard.com.br/wp-content/uploads/2017/01/000_par7224386-e1484392726406.jpg?resize=700%2C447"},
        "aberto": false,
        "mesas":4,
        "mesasDisponiveis": 3,
        "sobre":"Toalhas quentes! Temos toalhas quentes! Há 18 anos no ramo da barbearia, seguimos nosso código de conduta como nosso serviço prestado, impecável. Venha provar sabores e experiências, e quem sabe, no meio tempo, cortar o cabelo ou fazer a barba. O fliperama é free!!",
        "pessoasNaFila": 0,
        "posicaoNaFila": null
    }
];