import React from 'react';
import { Icon } from 'galio-framework';

class IconExtra extends React.Component {
  state = {
    fontLoaded: false
  };


  render() {
    const { name, family, ...rest } = this.props;

    return <Icon name={name} family={family} {...rest} />;
    
  }
}

export default IconExtra;
