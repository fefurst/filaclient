import React from "react";
import { StyleSheet, TouchableOpacity, Linking } from "react-native";
import { Block, Text, theme } from "galio-framework";

import Icon from "./Icon";
import {apptheme} from "../Constants";

//class DrawerItem extends React.Component {
 const DrawerItem = (props) => {
  
  const renderIcon = () => {
    const { title, focused } = props;

    switch (title) {
      case "Fila":
        return (
          <Icon
            name="local-restaurant"
            family=""
            size={18}
            color={focused ? apptheme.COLORS.PRIMARY : "white"}
            style={{ opacity: 0.5 }}
          />
        );
      case "Estabelecimentos":
        return (
          <Icon
            name="stacked-bar-chart"
            family=""
            size={18}
            color={focused ? apptheme.COLORS.PRIMARY : "white"}
            style={{ opacity: 0.5 }}
          />
        );
      case "Check-in":
        return (
          <Icon
            name="qr-code"
            family=""
            size={18}
            color={focused ? apptheme.COLORS.PRIMARY : "white"}
            style={{ opacity: 0.5 }}
          />
        );
      default:
        return null;
    }
  };


  const { focused, title, navigation } = props;

  const containerStyles = [
    styles.defaultStyle,
    focused ? [styles.activeStyle, styles.shadow] : null
  ];

  return (
    <TouchableOpacity
      style={{ height: 60 }}
      onPress={() => {  navigation.navigate(title); }
      }
    >
      <Block flex row style={containerStyles}>
        <Block middle flex={0.1} style={{ marginRight: 5 }}>
          {renderIcon()}
        </Block>
        <Block row center flex={0.9}>
          <Text
            style={{
              fontFamily: "Montserrat-Regular",
              textTransform: "uppercase",
              fontWeight: "300"
            }}
            size={12}
            bold={focused ? true : false}
            color={focused ? apptheme.COLORS.PRIMARY : "white"}
          >
            {title}
          </Text>
        </Block>
      </Block>
    </TouchableOpacity>
  );

}

const styles = StyleSheet.create({
  defaultStyle: {
    paddingVertical: 15,
    paddingHorizontal: 14,
    color: "white"
  },
  activeStyle: {
    backgroundColor: apptheme.COLORS.WHITE,
    borderRadius: 30,
    color: "white"
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.1
  }
});

export default DrawerItem;
