import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import {
  getFirebase,
  actionTypes as rrfActionTypes
} from 'react-redux-firebase';
import { constants as rfConstants } from 'redux-firestore';
import user from './features/user/userSlice';
import userEmpresa from './features/userEmpresa/userEmpresaSlice';


const extraArgument = {
    getFirebase
};

const middleware = [
    ...getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [
          ...Object.keys(rfConstants.actionTypes).map(
            type => `${rfConstants.actionsPrefix}/${type}`
          ),
          ...Object.keys(rrfActionTypes).map(
            type => `@@reactReduxFirebase/${type}`
          )
        ],
        ignoredPaths: ['firebase', 'firestore']
      },
      thunk: {
        extraArgument
      }
    })
  ];


const reducer = combineReducers({
  user,
  userEmpresa
});


export default configureStore({
  reducer,
  middleware
});