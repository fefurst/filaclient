import { createSlice } from '@reduxjs/toolkit'

const dummyData = {
    "aberto" : false,
    "id" : "0",
    "imagembg" : "https://i.vimeocdn.com/portrait/12134307_100x100",
    "imagempr" : "https://i.vimeocdn.com/portrait/12134307_100x100",
    "mesa" : [],
    "fila" : [],
    "mesas" : 0,
    "nome" : "Não está em fila alguma",
    "sobre" : "Entre em uma fila nos Estabelecimentos"
};

const userEmpresaSlice = createSlice({
  name: 'userEmpresa',
  initialState: {
      id: null,
      data: dummyData,
      imagembg: null, 
      imagempr:null
  },
  reducers: {
    setUserEmpresaId: (state, action) => {
      return {...state, id: action.payload};
    },
    setUserEmpresaData: (state, action) => {
      return {...state, data: action.payload};
    },
    setUserEmpresaImagemBG: (state, action) => {
      return {...state, imagembg: action.payload};
    },
    setUserEmpresaImagemPR: (state, action) => {
      return {...state, imagempr: action.payload};
    },
    limparUserEmpresa: (state, action) => {
      return {...state, data: dummyData, id: null, imagembg: null, imagempr:null};
    }
  }
});

export const getUserEmpresa = state => state.userEmpresa;

export const { 
  setUserEmpresaId, 
  setUserEmpresaData, 
  setUserEmpresaImagemBG, 
  setUserEmpresaImagemPR, 
  limparUserEmpresa } = userEmpresaSlice.actions

export default userEmpresaSlice.reducer