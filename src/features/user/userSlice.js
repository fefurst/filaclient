import { createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
  name: 'user',
  initialState: {
      data: {uid:null}
  },
  reducers: {
    setUser: (state, action) => {
      return {...state, data: action.payload};
      
    },
    limparUser: (state, action) => {
      return {...state, data: null};
    }
  }
});


export const getUser = state => state.user.data;

export const { setUser, limparUser } = userSlice.actions

export default userSlice.reducer