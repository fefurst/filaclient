import React, {useState, useEffect} from 'react';
import { StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import store from "./src/store";

import { Block, GalioProvider } from 'galio-framework';
import { NavigationContainer } from '@react-navigation/native';


import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';
import { useSelector, useDispatch } from 'react-redux';
import {
  setUser,
  getUser
} from './src/features/user/userSlice';
import {
  setUserEmpresaId,
  limparUserEmpresa,
  setUserEmpresaData,
  setUserEmpresaImagemBG,
  setUserEmpresaImagemPR
} from './src/features/userEmpresa/userEmpresaSlice'

import auth from '@react-native-firebase/auth';



import {apptheme} from './src/Constants';
import Splash from './src/screens/Splash';

import Screens from './src/navigation/Screens';

const App = () => {

  const dispatch = useDispatch();
  const user = useSelector(getUser);

  const onAuthStateChanged = (u) => {

    if(u == null) {
      auth().signInAnonymously();
    }
    dispatch(setUser({uid: u.uid}));    
  }

  const onValueChange = database()
    .ref(`/usuarios/${user.uid}`)
    .on('value', snapshot => {
      if(snapshot.val() != null) {
        dispatch(setUserEmpresaId(snapshot.val()));
        
        database()
        .ref(`/empresas/${snapshot.val()}`)
        .once("value")
        .then(esnapshot => {
            if(esnapshot.val() != null) {
              dispatch(setUserEmpresaData(esnapshot.val()));
              storage()
                .ref(esnapshot.val().imagempr)
                .getDownloadURL()
                .then((o)=>{
                  dispatch(setUserEmpresaImagemPR({uri:o}));
                });
              storage()
                .ref(esnapshot.val().imagembg)
                .getDownloadURL()
                .then((o)=>{
                  dispatch(setUserEmpresaImagemBG({uri:o}));
                });
            }
        }); 

      }
      else {
        dispatch(limparUserEmpresa());
      } 
        
  });

  useEffect(()=>{
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);

    database()
            .ref(`/usuarios/${user.uid}`)
            .off('value', onValueChange);

    return subscriber;
  }, [dispatch]);

  const [splashing, setSplashing] = useState(true);

  if(splashing) {
    return (
      <>
        <Splash onFinished={() => { setSplashing(false); } } delay={1000} />
      </>);
  }
  
  return (
    <>
      <StatusBar style="auto" translucent backgroundColor="transparent" />
      <NavigationContainer>
        <GalioProvider theme={apptheme}>
            <Block flex>
                <Screens />
            </Block>
          </GalioProvider>
      </NavigationContainer>
    </>
  );    
};

const AppParent = () => {
  return (
      <Provider store={store}>
        <App/>
      </Provider>
  );
}


export default AppParent;